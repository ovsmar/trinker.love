module.exports = {
    title: function () {
        return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow
    },

    line: function (title = "=") {
        return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black
    },

    allMale: function (p) {
        let n = [];
        for (x of p) {
            if (x.gender === "Male") {
                n.push(x);
            }
        }
        return n;

    },

    allFemale: function (p) {
        let n = [];
        for (x of p) {
            if (x.gender === "Female") {
                n.push(x);
            }
        }
        return n;

    },

    //**!LEVEL 1**//
   //**!Nombre d'hommes**//

    nbOfMale: function (p) {
        let n = 0
        for (let x of p) {
            if (x.gender === "Male") n++
        }
        return n
    },


    //**!Nombre de femmes**//

    nbOfFemale: function (p) {
        let n = 0
        for (let x of p) {
            if (x.gender === "Female") n++
        }
        return n
    },


    //**!Nombre de personnes qui cherchent un homme**//

    nbOfMaleInterest: function (p) {
        let n = 0
        for (let i of p) {
            if (i.looking_for === "M") {
                n++
            }
        }

        return n
    },

    //**!Nombre de personnes qui cherchent une femme**//

    nbOfFemaleInterest: function (p) {
        let n = 0
        for (let i of p) {
            if (i.looking_for === "F") {
                n++
            }
        }

        return n
    },

    //**!Nombre de personnes qui gagnent plus de 2000$**//

    nbOIncomePlus2000: function (p) {
        let n = 0;
        for (let x of p) {
            result = x.income;
            value = parseFloat(result.slice(1));
            if (value > 2000) {
                n++
            }
        };
        return n;
    },

    //**!Nombre de personnes qui aiment les Drama**//

    nbOfDrama: function (p) {
        let n = 0;
        for (let x of p) {
            if (x.pref_movie === "Drama") n++
        }
        return n;
    },

    //**!Nombre de femmes qui aiment la science-fiction**//

    nbOfFemaleSF: function (p) {
        let n = 0;
        for (let x of p) {
            if (x.gender === 'Female' && x.pref_movie.includes('Sci-Fi')) n++
        };
        return n;
    },

    //**!LEVEL 2**//
   //**!Nombre de personnes qui aiment les documentaires et gagnent plus de 1482$**//

    nbOfDtsPlus1482: function (p) {
        let n = 0;
        for (let x of p) {
            result = x.income;
            value = parseFloat(result.slice(1));
            if (x.pref_movie.includes("Documentary") && value > 1482) {
                n++
            }
        };
        return n;
    },
    //**!Liste des noms, prénoms, id et revenu des personnes qui gagnent plus de 4000$**//

    nbOfNaPsIdPlus4000: function (p) {
        let n = [];
        for (let x of p) {
            y = x.income.substring(1)
            if (y > 4000) {
                let k = [x.last_name, x.last_name, x.id, x.income]
                n.push(k);

            }
        }
        return n;
    },

    //**!Homme le plus riche (nom et id)**//

    nb0fRiche: function (p) {

        let hommeleplusriche = 0;
        for (let x of p) {
            result = x.income;
            value = parseFloat(result.slice(1));
            if (value > hommeleplusriche && x.gender == "Male") {
                hommeleplusriche = value;
                id_richesse = x.id;
                name_richesse = x.last_name;
            }
        }
        return "ID : " + id_richesse + " Nom : " + name_richesse + " Revenu : " + hommeleplusriche + "$";
    },

    //**!Salaire moyen**//

    nb0fSaMoy: function (p) {
        let salairtotal = 0
        for (let x of p) {
            result = x.income;
            value = parseFloat(result.slice(1));
            salairtotal += value

        }
        return salairtotal / 1000;
    },

    //**!Salaire médian**//

    nb0fSaMed: function (p) {
        let tab_salaire = []
        for (let x of p) {
            result = x.income;
            value = parseFloat(result.slice(1));
            tab_salaire.push(value);
        }
        tab_salaire = tab_salaire.sort((a, b) => a - b);
        return tab_salaire[tab_salaire.length / 2]
    },

    //**!Nombre de personnes qui habitent dans l'hémisphère nord**//

    nb0PerNor: function (p) {
        let n = 0;
        for (let x of p) {

            if (x.latitude > 0) {
                n++
            }

        }
        return n;

    },

    //**!Salaire moyen des personnes qui habitent dans l'hémisphère sud**//

    nb0PerSalSud: function (p) {

        let salairtotal = 0
        for (let x of p) {
            if (x.latitude > 0) {
                result = x.income;
                value = parseFloat(result.slice(1));
                salairtotal += value
            }


        }
        return salairtotal / 1000;


    },

    match: function (p) {
        return "bob"
    },

    //**!LEVEL 3**//
   //**!Personne qui habite le plus près de Bérénice Cawt (nom et id)**//

    nb0PerPresCaw: function (p) {
        let dist = [];
        let latberenice = 15.5900396;
        let longberenice = -87.879523;
        let distance_plus_petite_trouver = 99999999999999999999n;
        let le_plus_proche
        for (let x of p) {
            /// a et b ?
            let a = latberenice - x.latitude
            let b = longberenice - x.longitude
            let distance = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            if ((distance < distance_plus_petite_trouver) && distance !== 0) {
                distance_plus_petite_trouver = distance;
                le_plus_proche = x;
            }
        }
        return le_plus_proche.last_name + " " + le_plus_proche.id
    },

    //**!Personne qui habite le plus près de Ruì Brach (nom et id)**//

    nb0PerPresBrach: function (p) {
        let dist = [];
        let latRuì = 33.347316;
        let longRuì = 120.16366;
        let distance_plus_petite = 99999999999999999999n;
        let plus_proche
        for (let x of p) {
            let a = latRuì - x.latitude
            let b = longRuì - x.longitude
            let distance = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            if ((distance < distance_plus_petite) && distance !== 0) {
                distance_plus_petite = distance;
                plus_proche = x;
            }

        }
        return plus_proche.last_name + " " + plus_proche.id
    },

    //**!les 10 personnes qui habite les plus près de Josée Boshard (nom et id)**//

    nb0Per10PresBoshard: function (p) {

        let dist = [];
        let latBoshard = 57.6938555
        let longBoshard = 11.9704401
        for (let x of p) {
            let a = latBoshard - x.latitude
            let b = longBoshard - x.longitude
            let distance_entre_eux = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
            dist.push({
                distance: distance_entre_eux,
                nom: x.last_name,
                id: x.id
            })
            dist.sort((a, b) => a.distance - b.distance)
        }

        let liste_10 = []
        for (machin = 1; machin < 11; machin++) {
            liste_10.push(dist[machin]);
        }

        return liste_10;

    },

    //**!Les noms et ids des 23 personnes qui travaillent chez google**//

    nb0Per23Google: function (p) {
        let list = [];
        const google = p.filter(p => p.email.includes('google'))
        for (let x of google) {
            list.push({
                name: x.last_name,
                id: x.id
            })
        }
        return list;
    },

    //**!Personne la plus agée**//

    nb0Agee: function (p) {
        let people = [];
        for (let x of p) {
            people.push({
                name: x.last_name,
                année: (new Date(x.date_of_birth))
            });
            people.sort(function (a, b) {
                return a.année - b.année
            });
        }
        return people[0];
    },

    //**!Personne la plus jeune**//

    nb0Young: function (p) {
        let people = [];
        for (let x of p) {
            people.push({
                name: x.last_name,
                année: (new Date(x.date_of_birth))
            });
            people.sort(function (a, b) {
                return b.année - a.année
            });
        }
        return people[0];
    },

    //**!Moyenne des différences d'age**//
   // Pas fini//

    nb0MoyDifAge: function (p) {
        let sommedifage = 0;
        let compteur = 0;

        for (first_peopls of p) {
            let dob = first_peopls.date_of_birth;
            let age = ((Date.now() - new Date(dob)) / 31557600000);

            for (second_peopls of p) {
                let dob = second_peopls.date_of_birth;
                let age2 = ((Date.now() - new Date(dob)) / 31557600000);

                if (age > age2) {
                    sommedifage = sommedifage + (age - age2)
                    compteur++;


                } else {
                    sommedifage = sommedifage + (age2 - age)
                    compteur++;

                }

            }
        }

        return (sommedifage / compteur);

    },


    //**!LEVEL 4**//
   //**!Genre de film le plus populaire**/
  // Pas fini//

    nb0FilmPopulaire: function (p) {


        let film = [];
        for (let x of p) {
            film.push({
                genre: x.pref_movie,
                genre: x.pref_movie,
            });
            film.sort(function (a, b) {
                return (a.film - b.film);
            });
        }
        return film[0];


    },
    //**!Genres de film par ordre de popularité **/
   // Pas fini//

    nb0FilmPopulaireOrdre: function (p) {

        let film = []
        for (let x of p) {
            film.push({
                parordre: x.pref_movie,

            });
            film.sort((a, b) => b - a);


        }
        //return film ;
    },





    nb0FilmPersPrefer: function (p) {

    },


    //**!calc age pour ↓↓ageMoyenHomFilmNoir↓↓ **/

    calcAge: function (p) {
        let dob = p.date_of_birth;
        let birthday = new Date(dob);
        return ~~((Date.now() - birthday) / 31557600000);
    },



    //**!Age moyen des hommes qui aiment les films noirs**/

    ageMoyenHomFilmNoir: function (p) {
        total = [];
        const reducer = (previousValue, currentValue) =>
            previousValue + currentValue;
        for (let i of this.allMale(p)) {
            if (i.pref_movie.includes("Film-Noir")) {
                total.push(this.calcAge(i));
            }
        }

        return total.reduce(reducer) / total.length;


    },
    //**!Age moyen des femmes qui aiment les films noirs, habitent sur le fuseau horaire de Paris et gagnent moins que la moyenne des hommes**//


    ageMoyenFemmesFilmNoir: function (p) {


    },
}